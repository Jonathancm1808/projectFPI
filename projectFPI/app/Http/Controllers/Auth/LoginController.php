<?php

namespace App\Http\Controllers\Auth;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    public function showLoginForm()
    {
        return view('auth.login');
    }
    public function login()
    {
        $credentials = $this->validate(request(),[
            'email' => 'requiered|',
            'password' => 'requiered|'
        ]);

        if(Auth::attempt([$credentials]))
        {
            return redirect()->route('dashboard');
        }
        return $credentials;


        return back()
          ->withErrors(['email' => trans('auth.failed')])
          ->withInput(request(['email']));
    }
}
