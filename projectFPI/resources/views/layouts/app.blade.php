<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
  </head>
  <body id="cuerpo">
    <div class="container">
      @yield('content')
    </div>
  </body>
</html>
