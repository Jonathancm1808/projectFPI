@extends('layouts.app')

@section('content')
      <div id="lol" class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card panel-default">
              <div id="top" class="card-group">
                <img class=" mx-auto" src="images/logo.jpg" width="110" height="150">
              </div>
                <div class="card-body">
                  <div class="card-heading">
                    <h1 class="card-title text-center">Iniciar Sesion</h1>
                  </div>
                </div>
                <div class="card-body">
                    <form  class="form-signin" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label for="inputemail">Email</label>
                            <input id="inputEmail" type="email" name="email" class="form-control" placeholder="Ingresa tu email" value="{{ old('email') }}">
                           {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <label for="inputPassword">Password</label>
                            <input id="inputPassword" type="password" name="password" class="form-control" placeholder="Ingresa tu contraseña">
                           {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                        </div>
                        <hr class="my-4">
                        <button id="envio" class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="button">Ingresar</button>
                    </form>
                </div>
            </div>
        </div>
      </div>
@endsection
