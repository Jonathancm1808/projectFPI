@extends('layouts.app')

@section('content')
    <body id="todo">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                    @endauth
                </div>
            @endif

            <div id="title" class="content">
                <div class="title m-b-md">
                    SIGFPI
                </div>
            </div>
        </div>
    </body>
@endsection
